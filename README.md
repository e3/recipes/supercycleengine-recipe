# supercycleEngine conda recipe

Home: "https://github.com/icshwi/supercycleEngine"

Package license: LGPL-2.1

Recipe license: BSD 3-Clause

Summary: ESS Site-specific supercycleEngine EPICS module
